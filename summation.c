//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Cassidy Siegrist <cjjs@hawaii.edu>
// @date   12_1_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]);
   int a,b;
        for(a = 1; a <= n; a = a+1){
               b = b + a;
        }
   printf("The sum of the digits from 1 to %d is %d\n", n, b);

        return 0;
}
